package com.juandavid.totalplayevaluacion.utils

import android.content.Context
import android.content.SharedPreferences

object UserLoginManager {
    private const val PREF_NAME = "UserLoginPrefs"

    private const val KEY_IS_LOGGED_IN = "isLoggedIn"
    private const val PREF_SESSION = "session"

    fun setLoggedIn(context: Context, isLoggedIn: Boolean) {
        val sharedPrefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPrefs.edit()
        editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn)
        editor.apply()
    }

    fun saveSession(context: Context, session: String) {
        val sharedPrefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPrefs.edit()
        editor.putString(PREF_SESSION, session)
        editor.apply()
    }

    fun isLoggedIn(context: Context): Boolean {
        val sharedPrefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        return sharedPrefs.getBoolean(KEY_IS_LOGGED_IN, false)
    }

}
