package com.juandavid.totalplayevaluacion

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TotalPlayApplication:Application()