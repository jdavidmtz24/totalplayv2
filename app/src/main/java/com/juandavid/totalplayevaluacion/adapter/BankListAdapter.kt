package com.juandavid.totalplayevaluacion.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.juandavid.totalplayevaluacion.R
import com.juandavid.totalplayevaluacion.data.model.ArrayReferences
import com.juandavid.totalplayevaluacion.data.model.Images
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception

class BankListAdapter(arrayReferences: ArrayList<ArrayReferences>, private val onClickListener: OnClickListener): RecyclerView.Adapter<BankListAdapter.ViewHolder>(){

    private val modelArrayReferences: ArrayList<ArrayReferences>
    class ViewHolder(item: View): RecyclerView.ViewHolder(item){
         val itemImageBank: ImageView
         val itemTextReferentBank: TextView
         val itemProgressBar:ProgressBar
        init {
            itemImageBank = item.findViewById(R.id.image_bank_item)
            itemTextReferentBank = item.findViewById(R.id.text_reference_bank_item)
            itemProgressBar = item.findViewById(R.id.progressbar_item)
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_bank_item, parent, false)
        return ViewHolder(view)
    }
    override fun getItemCount(): Int {
      return modelArrayReferences.size
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemProgressBar.isVisible = true
        val arrayReference = modelArrayReferences[position]
        try {
            Log.e("model"," modelArray ${modelArrayReferences.size} position $position")
            if(arrayReference.images?.isNotEmpty()!!) {
                images(arrayReference.images[0])
                val urlImageBank: String? = images(arrayReference.images[0])//arrayReference.images[arrayReference.images.size-1].url3X3
                if (urlImageBank != null) {
                    Log.e("imageURL",urlImageBank)
                }
                Picasso.get().load(urlImageBank).into(holder.itemImageBank, object :Callback{
                    override fun onSuccess() {
                        holder.itemProgressBar.isVisible = false
                    }

                    override fun onError(e: Exception?) {
                        holder.itemProgressBar.isVisible = false
                    }

                })
                holder.itemProgressBar.isVisible = false
                holder.itemTextReferentBank.text =  arrayReference.reference
                holder.itemView.setOnClickListener {
                    onClickListener.clickListener(arrayReference)
                }
            }
        } catch (nullpointer:java.lang.NullPointerException) {
            holder.itemProgressBar.isVisible = false
        }

    }
    private fun images(images: Images):String?{
        if (images.url3X3!=null){
            return images.url3X3
        }
        else if (images.url4X4!=null){
            return images.url4X4
        }
        else if (images.url5X5!=null){
            return images.url5X5
        }
        else if (images.url6X6!=null){
            return images.url6X6
        }
        else if (images.url7X7!=null){
            return images.url7X7
        }
        return null
    }
    init {
        this.modelArrayReferences = arrayReferences
    }
    class OnClickListener(val clickListener: (arrayReference: ArrayReferences) -> Unit){
        fun onClick(arrayReference: ArrayReferences)= clickListener(arrayReference)
    }
}