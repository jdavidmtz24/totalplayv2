package com.juandavid.totalplayevaluacion.ui.login.ui.login


data class LoginResult(
    val success: LoggedInUserView? = null,
    val error: Int? = null
)