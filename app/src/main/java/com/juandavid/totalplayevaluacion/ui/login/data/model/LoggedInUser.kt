package com.juandavid.totalplayevaluacion.ui.login.data.model

/**
 * Data class that captures user information for logged in users retrieved from TotalPlayRepository
 */
data class LoggedInUser(
    val userId: String,
    val displayName: String
)