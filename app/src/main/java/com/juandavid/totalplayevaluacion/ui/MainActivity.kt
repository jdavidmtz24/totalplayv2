package com.juandavid.totalplayevaluacion.ui

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.snackbar.Snackbar
import com.juandavid.totalplayevaluacion.R
import com.juandavid.totalplayevaluacion.databinding.ActivityMainBinding
import com.juandavid.totalplayevaluacion.ui.listbank.BankFragment
import com.juandavid.totalplayevaluacion.utils.ConnectionType
import com.juandavid.totalplayevaluacion.utils.NetworkMonitorUtil
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val networkMonitorUtil = NetworkMonitorUtil(this)
    private lateinit var binding: ActivityMainBinding
    val viewModel: MainActivityViewModel by viewModels()
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.containerFragment) as NavHostFragment
        navHostFragment.navController
        networkState()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun networkState() {
        networkMonitorUtil.result = { isAvailable, type ->
            runOnUiThread {
                when (isAvailable) {
                    true -> {
                        when (type) {
                            ConnectionType.Wifi -> {
                                Log.i("NETWORK_MONITOR_STATUS", "Wifi Connection")
                               /* Snackbar.make(binding.coordinatorLayout, getText(R.string.network_ok), Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show()*/
                                val snackbar = Snackbar.make(binding.coordinatorLayout, getText(R.string.network_ok),
                                    Snackbar.LENGTH_LONG)
                                    .setAction("Action", null)
                                snackbar.setBackgroundTint(getColor(R.color.connection_green))
                                val sbView: View = snackbar.view
                                val textView = sbView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
                                textView.setTextColor(Color.WHITE)
                                snackbar.show()
                                viewModel.isConnetion.value= true
                            }
                            ConnectionType.Celular -> {
                                Log.i("NETWORK_MONITOR_STATUS", "Celular Connection")
                                val snackbar = Snackbar.make(binding.coordinatorLayout, getText(R.string.network_ok),
                                    Snackbar.LENGTH_LONG)
                                    .setAction("Action", null)
                                snackbar.setBackgroundTint(getColor(R.color.connection_green))
                                val sbView: View = snackbar.view
                                val textView = sbView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
                                textView.setTextColor(Color.WHITE)
                                snackbar.show()
                                viewModel.isConnetion.value= true
                            }
                            else -> {

                            }
                        }
                    }
                    false -> {
                        Log.i("NETWORK_MONITOR_STATUS", "No Connection")
                        val snackbar = Snackbar.make(binding.coordinatorLayout, getText(R.string.network_failed),
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction("Action", null)
                        snackbar.setBackgroundTint(getColor(R.color.connection_red))
                        val sbView: View = snackbar.view
                        val textView = sbView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
                        textView.setTextColor(Color.WHITE)
                        snackbar.show()
                        viewModel.isConnetion.value= true
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        networkMonitorUtil.register()
    }

    override fun onStop() {
        super.onStop()
        networkMonitorUtil.unregister()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.containerFragment)
        if (fragment is BankFragment)
            return super.onBackPressed()
    }
}