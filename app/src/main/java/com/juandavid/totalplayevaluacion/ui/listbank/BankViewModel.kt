package com.juandavid.totalplayevaluacion.ui.listbank

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.juandavid.totalplayevaluacion.data.model.ArrayReferences
import com.juandavid.totalplayevaluacion.data.model.SessionRequest
import com.juandavid.totalplayevaluacion.domain.GetListBankUsesCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BankViewModel @Inject constructor(private var getListBankUsesCases : GetListBankUsesCases) : ViewModel() {
    val isLoading = MutableLiveData<Boolean>()
    val listBanck= MutableLiveData<ArrayList<ArrayReferences>?>()

    fun onCreate(session: String?) {
         viewModelScope.launch {
             isLoading.postValue(true)
             if (!session.isNullOrBlank()) {
                 val result = session?.let { SessionRequest(it) }
                     ?.let { getListBankUsesCases.invoke(it) }
                 if (result != null) {
                     if (result.size>=1) {
                         listBanck.postValue(result)
                     }
                 }
             }
             isLoading.postValue(false)
         }
    }
}