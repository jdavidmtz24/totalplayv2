package com.juandavid.totalplayevaluacion.ui.listbank

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.juandavid.totalplayevaluacion.R
import com.juandavid.totalplayevaluacion.adapter.BankListAdapter
import com.juandavid.totalplayevaluacion.databinding.FragmentBankBinding
import com.juandavid.totalplayevaluacion.utils.UserLoginManager
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BankFragment : Fragment() {

    companion object {
        fun newInstance() = BankFragment()
    }

    private lateinit var viewModel: BankViewModel
    private lateinit var binding: FragmentBankBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBankBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(BankViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pref: SharedPreferences = requireContext().getSharedPreferences("Session", Context.MODE_PRIVATE)
        val session = pref.getString("session","5zq7zyI/kXyWZqCQ68CK9bJ7ezvO9mNuRpiuKFcnthAr7ODPEaI9tBGL9/FgKIKD")
        session?.let { Log.e(" BankFragment.session" , it) }
        viewModel.onCreate(session)
        viewModel.isLoading.observe(viewLifecycleOwner){
            binding.progressbarListBank.isVisible = it
        }
        dateRecyclerList()

        binding.logoutButton.setOnClickListener{
            UserLoginManager.setLoggedIn(requireContext(), false)
            view.findNavController().navigate(R.id.action_bankFragment_to_loginFragment)
            Toast.makeText(requireContext(), getString(R.string.logout_success), Toast.LENGTH_LONG).show()
        }
    }

    private fun dateRecyclerList() {
        viewModel.listBanck.observe(viewLifecycleOwner){
            if (it != null) {
                if (it.isNotEmpty()) {
                    val listBankAdapter = BankListAdapter(it,BankListAdapter.OnClickListener{

                    })
                    binding.recyclerBank.adapter = listBankAdapter
                    binding.recyclerBank.layoutManager = LinearLayoutManager(context)
                    binding.recyclerBank.setHasFixedSize(true)
                }
            }

        }
    }

}