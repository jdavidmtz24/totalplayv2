package com.juandavid.totalplayevaluacion.domain

import com.juandavid.totalplayevaluacion.data.TotalPlayRepository
import com.juandavid.totalplayevaluacion.data.model.ArrayReferences
import com.juandavid.totalplayevaluacion.data.model.SessionRequest
import com.juandavid.totalplayevaluacion.data.model.SessionResponse
import javax.inject.Inject


class GetListBankUsesCases @Inject constructor(private val repository : TotalPlayRepository){


    suspend operator fun invoke(sessionRequest: SessionRequest): ArrayList<ArrayReferences>? = repository.doPostListBank(sessionRequest)
}