package com.juandavid.totalplayevaluacion.data.model

import com.google.gson.annotations.SerializedName

data class Images(
    @SerializedName("url3X3") val url3X3: String?= null,
    @SerializedName("url4X4") val url4X4: String?= null,
    @SerializedName("url5X5") val url5X5: String?= null,
    @SerializedName("url6X6") val url6X6: String?= null,
    @SerializedName("url7X7") val url7X7: String?= null
)
