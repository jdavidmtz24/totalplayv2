package com.juandavid.totalplayevaluacion.data.model

import com.google.gson.annotations.SerializedName

data class LoginRequest(@SerializedName("user") val user:String,
                        @SerializedName("password") val password:String)
