package com.juandavid.totalplayevaluacion.data.network

import android.util.Log
import com.juandavid.totalplayevaluacion.core.RetrofitHelper
import com.juandavid.totalplayevaluacion.data.model.LoginRequest
import com.juandavid.totalplayevaluacion.data.model.LoginResponse
import com.juandavid.totalplayevaluacion.data.model.SessionRequest
import com.juandavid.totalplayevaluacion.data.model.SessionResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class TotalPlayService  @Inject constructor( private val api:ApiClient){



    suspend fun postLogin(loginRequest: LoginRequest):LoginResponse?{
       return withContext(Dispatchers.IO){
           val response = api.doLogin(loginRequest)
           Log.e("postLogin","Codigo ${response.code()}")
           if (response.code() == 200) response.body() else null
       }

    }

    suspend fun postListBank(sessionRequest: SessionRequest):SessionResponse?{
        return withContext(Dispatchers.IO){
            val response = api.doAllBank(sessionRequest)
            Log.e("postLogin","Codigo ${response.code()}")
            if (response.code() == 200) response.body() else null
        }
    }
}