package com.juandavid.totalplayevaluacion.data.model

import com.google.gson.annotations.SerializedName

data class SessionRequest(@SerializedName("session") val session:String)
