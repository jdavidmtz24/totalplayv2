package com.juandavid.totalplayevaluacion.data.network

import com.juandavid.totalplayevaluacion.data.model.LoginRequest
import com.juandavid.totalplayevaluacion.data.model.LoginResponse
import com.juandavid.totalplayevaluacion.data.model.SessionRequest
import com.juandavid.totalplayevaluacion.data.model.SessionResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface ApiClient {

    @Headers("Content-Type: application/json")
    @POST("cliente.do")
    suspend fun doLogin(@Body loginRequest: LoginRequest):Response<LoginResponse>

    @Headers("Content-Type: application/json")
    @POST("clienteresp.do")
    suspend fun doAllBank(@Body sessionRequest: SessionRequest):Response<SessionResponse>
}