package com.juandavid.totalplayevaluacion.data.model

import com.google.gson.annotations.SerializedName

data class ArrayReferences(@SerializedName("images") val images:ArrayList<Images>? = arrayListOf(),
                           @SerializedName("bank") val bank:String,
                           @SerializedName("reference") val reference:String,
                           @SerializedName("aliasbank") val aliasbank:String)
