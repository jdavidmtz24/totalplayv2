package com.juandavid.totalplayevaluacion.data

import com.juandavid.totalplayevaluacion.data.model.*
import com.juandavid.totalplayevaluacion.data.network.TotalPlayService
import javax.inject.Inject


class TotalPlayRepository @Inject constructor(private val api: TotalPlayService){

    suspend fun postLogin(loginRequest: LoginRequest): LoginResponse? {
        val response = api.postLogin(loginRequest)
        LoginProvider.loginResponse = response
        return response
    }

    suspend fun doPostListBank(sessionRequest: SessionRequest):ArrayList<ArrayReferences>?{
        val response = api.postListBank(sessionRequest)
        if (response != null) {
            if (response.status==0) {
                BankProvider.loginResponse = response
                return response.arrayReferences
            }
        }
        return null
    }
}